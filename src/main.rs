use orbtk::*;

struct MainView;

impl Widget for MainView {
    fn create() -> Template {
        let navbar = Row::create()
            .with_child(Button::create().with_property(Label::from("basic")))
            .with_child(Button::create().with_property(Label::from("Science")));
        let input =
            Row::create().with_child(TextBox::create().with_property(WaterMark::from("input")));
        Template::default()
            .as_parent_type(ParentType::Single)
            .with_child(Column::create().with_child(navbar).with_child(input))
    }
}

fn main() {
    let mut app = Application::default();

    app.create_window()
        .with_bounds(Bounds::new(50, 50, 400, 300))
        .with_title("Calculator")
        .with_root(MainView::create())
        .with_resizable(true)
        .with_debug_flag(false)
        .build();

    app.run();
}
